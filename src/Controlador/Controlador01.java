


package Controlador;

/**
 *
 * @author Josue Daniel Roldan Ochoa 
 * 
 */


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class Controlador01 {
private String rs;
private JFrame fm1 = new JFrame ();
private JMenuBar br= new JMenuBar();
private JFrame fm2= new JFrame();
private JTextField fl1= new JTextField();
private JTextField fl2= new JTextField();
private JTextField fl3= new JTextField();
private JTextField fl4= new JTextField();
private JTextField fl5= new JTextField();
private JComboBox cb1= new JComboBox();
private String [] ary= new String[160];
private JButton bt1= new JButton("Aceptar");
private int a1;
private int a2;
private int a3;
private int a4;

public void Control(){
fl1.setBounds(30,20,30,25);
fl2.setBounds(60,20,30,25);
fl3.setBounds(90,20,30,25);
fl4.setBounds(120,20,30,25);
cb1.setBounds(150,20,180,25);
bt1.setBounds(30,45,300,20);
fm2.add(bt1);
fm2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
fm2.setResizable(false);
fm2.setBounds(980,600,360,110);
fm2.setLayout(null);
fm2.add(fl1);
fm2.add(fl2);    
fm2.add(fl3);
fm2.add(fl4);
fm2.add(cb1);
fm1.setLayout(null);
fm1.add(br);
fm1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
fm1.setResizable(false);
fm1.setBounds(0,0,0,0);
fm1.setVisible(true);
fm2.setVisible(true);

ary[0]="none";
ary[1]="Black";
ary[2]="Blue";
ary[3]="Cyan";
ary[4]="Dark Gray";
ary[5]="Gray";
ary[6]="Green";
ary[7]="Light Gray";
ary[8]="Magenta";
ary[9]="Orange";
ary[10]="Pink";
ary[11]="Red";
ary[12]="White";
ary[13]="Yellow";
       
cb1.addItem(ary[0]);
cb1.addItem(ary[1]);
cb1.addItem(ary[2]);
cb1.addItem(ary[3]);
cb1.addItem(ary[4]);
cb1.addItem(ary[5]);
cb1.addItem(ary[6]);
cb1.addItem(ary[7]);
cb1.addItem(ary[8]);
cb1.addItem(ary[9]);
cb1.addItem(ary[10]);
cb1.addItem(ary[11]);
cb1.addItem(ary[12]);
cb1.addItem(ary[13]);
fl1.setText("300");
fl2.setText("200");
fl3.setText("300");
fl4.setText("250");
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
fl1.addKeyListener(new KeyAdapter(){
public void keyPressed(KeyEvent e){
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
}
});
fl2.addKeyListener(new KeyAdapter(){
public void keyPressed(KeyEvent e){
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
}
});
fl3.addKeyListener(new KeyAdapter(){
public void keyPressed(KeyEvent e){
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
}
});
fl4.addKeyListener(new KeyAdapter(){
public void keyPressed(KeyEvent e){
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
}
});
        
bt1.addActionListener(new ActionListener(){
@Override
public void actionPerformed(ActionEvent e) {
String rs= cb1.getSelectedItem().toString();
int a1= Integer.parseInt(fl1.getText());
int a2=  Integer.parseInt( fl2.getText());
int a3= Integer.parseInt(fl3.getText());
int a4=  Integer.parseInt(fl4.getText());
fm1.setBounds(a1,a2,a3,a4);
if(rs.equalsIgnoreCase("none")){
fm1.getContentPane().setBackground(null);
fm2.getContentPane().setBackground(null);
}
if(rs.equalsIgnoreCase("Black")){
fm1.getContentPane().setBackground(Color.BLACK);
fm2.getContentPane().setBackground(Color.BLACK);
}
if(rs.equalsIgnoreCase("Blue")){   
fm1.getContentPane().setBackground(Color.BLUE);
fm2.getContentPane().setBackground(Color.BLUE);
}
if(rs.equalsIgnoreCase("Cyan")){   
fm1.getContentPane().setBackground(Color.CYAN);
fm2.getContentPane().setBackground(Color.CYAN);
}
if(rs.equalsIgnoreCase("Dark Gray")){   
fm1.getContentPane().setBackground(Color.DARK_GRAY);
fm2.getContentPane().setBackground(Color.DARK_GRAY);
}
if(rs.equalsIgnoreCase("Gray")){   
fm1.getContentPane().setBackground(Color.GRAY);
fm2.getContentPane().setBackground(Color.GRAY);
}
if(rs.equalsIgnoreCase("Green")){   
fm1.getContentPane().setBackground(Color.GREEN);
fm2.getContentPane().setBackground(Color.GREEN);
}
if(rs.equalsIgnoreCase("Light Gray")){   
fm1.getContentPane().setBackground(Color.LIGHT_GRAY);
fm2.getContentPane().setBackground(Color.LIGHT_GRAY);
}
if(rs.equalsIgnoreCase("Magenta")){   
fm1.getContentPane().setBackground(Color.MAGENTA);
fm2.getContentPane().setBackground(Color.MAGENTA);
}
if(rs.equalsIgnoreCase("Orange")){   
fm1.getContentPane().setBackground(Color.ORANGE);
fm2.getContentPane().setBackground(Color.ORANGE);
}
if(rs.equalsIgnoreCase("Pink")){   
fm1.getContentPane().setBackground(Color.PINK);
fm2.getContentPane().setBackground(Color.PINK);
}
if(rs.equalsIgnoreCase("Red")){   
fm1.getContentPane().setBackground(Color.RED);
fm2.getContentPane().setBackground(Color.RED);
}
if(rs.equalsIgnoreCase("White")){   
fm1.getContentPane().setBackground(Color.WHITE);
fm2.getContentPane().setBackground(Color.WHITE);
}
if(rs.equalsIgnoreCase("Yellow")){   
fm1.getContentPane().setBackground(Color.YELLOW);
fm2.getContentPane().setBackground(Color.YELLOW);
}
}
});
}
}
